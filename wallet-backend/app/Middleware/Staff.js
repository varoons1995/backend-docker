'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Staff {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   * 
   * 
   */
  async handle ({ request }, next) {
    // call next to advance the request
    let a = await Database.raw('SELECT * FROM UFABET.tokens WHERE staff_id = ? ORDER BY id DESC LIMIT 1;',[v.ID])
    await next()
  }
}

module.exports = Staff
