FROM node:12.16

RUN npm install -g pm2
RUN npm install -g adonis-cli

WORKDIR /var/www/html

COPY ./wallet-backend /var/www/html
COPY ./wallet-backend/package.json /var/www/html/package.json

RUN cd /var/www/html
RUN npm install
