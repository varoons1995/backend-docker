'use strict'

const Database = use('Database')


class BankOwnerController {

    async FindAll(){
        const query = Database.table('BANK_OWNER')

        const Bank_OwnerAll = await query;
        console.log(Bank_OwnerAll)
        return Bank_OwnerAll;
    }

    async addBankOwner(request, response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('BANK_OWNER')
        const body = request._request_._original
        console.log(body)	 
        const addBankOwner = await query.insert({
            'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
            'BANK_ACCOUNT_NAME':body.BANK_ACCOUNT_NAME,
            'BANK_TYPE':body.BANK_TYPE,
            'CURRENT_BALANCE':body.CURRENT_BALANCE,
            'BANK_STATUS':body.BANK_STATUS,
            'BANK_TITLE':body.BANK_TITLE,
            'BANK_GEAR':body.BANK_GEAR,
            'BANK_USERNAME':body.BANK_USERNAME,
            'BANK_PASSWORD':body.BANK_PASSWORD,
            'CREATEDATE':dt,
            'UPDATEDATE':dt
        })
        if(addBankOwner){
            response = addBankOwner[0]
        }else{
            response = 'Error'
        }
        
        return {message : response}
    }

    async editBankOwner(request, response){
        const query = Database.table('BANK_OWNER')
        const body = request._request_._original
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        console.log(body)	
        try{
            const EditBankOwner = await query.where('ID',body.ID)
            .update({
                'BANK_ACCOUNT_NUMBER':body.BANK_ACCOUNT_NUMBER,
                'BANK_ACCOUNT_NAME':body.BANK_ACCOUNT_NAME,
                'BANK_TYPE':body.BANK_TYPE,
                'CURRENT_BALANCE':body.CURRENT_BALANCE,
                'BANK_STATUS':body.BANK_STATUS,
                'BANK_TITLE':body.BANK_TITLE,
                'BANK_GEAR':body.BANK_GEAR,
                'BANK_USERNAME':body.BANK_USERNAME,
                'BANK_PASSWORD':body.BANK_PASSWORD,
                'UPDATEDATE':dt
            })
            if(EditBankOwner){
                response = 'Success'
            }else{
                response = 'Error'
            }
            
            return {message : response}

        }catch(err){
            console.error(err)
        }
    }

}

module.exports = BankOwnerController
